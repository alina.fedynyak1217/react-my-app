import React from 'react';
import '../post/post.css';

const Post = ({post}: any) => {
    return (
        <div key={post.id} className='post-block'>
            <div>
                <img width='50px' height='50px' src={post.img} alt="post"/>
            </div>
            <div className='post-text'>
                <h2>{post.title}</h2>
                <p>{post.text}</p>
            </div>
        </div>
    )
};

export default Post;

/*
import { Component } from 'react';
import '../post/post.css';
import {PostArr} from "./Posts";

interface Props {
    post: PostArr,
}

class Post extends Component<Props> {
    constructor(props: Props) {
        super(props);
    }
    render() {
        return (
            <>
                <div key={this.props.post.id} className='post-block'>
                    <div>
                        <img width='50px' height='50px' src={this.props.post.img} alt="post"/>
                    </div>
                    <div className='post-text'>
                        <h2>{this.props.post.title}</h2>
                        <p>{this.props.post.text}</p>
                    </div>
                </div>
            </>
        )
    }
}

export default Post;
*/
