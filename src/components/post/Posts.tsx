export interface PostArr {
    id: number;
    img: string;
    title: string;
    text: string;
}

const Posts: PostArr[] = [
    {
        id: 1,
        img: 'https://pngimg.com/uploads/spongebob/spongebob_PNG32.png',
        title: 'Post 1',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    {
        id: 2,
        img: 'https://pngimg.com/uploads/spongebob/spongebob_PNG32.png',
        title: 'Post 2',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    {
        id: 3,
        img: 'https://pngimg.com/uploads/spongebob/spongebob_PNG32.png',
        title: 'Post 3',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    {
        id: 4,
        img: 'https://pngimg.com/uploads/spongebob/spongebob_PNG32.png',
        title: 'Post 4',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    }
];

export default Posts;

