import React from 'react';
import Post from "./Post";

const PostsBlock = ({posts}: any) => {
    return (
        <>
            {
                posts.map((item: any) => (
                    <div key={item.id}>
                        <Post post={item} />
                    </div>
                ))
            }
        </>
    )
};

export default PostsBlock;

/*import { Component } from 'react';
import Post from "./Post";
import {PostArr} from "./Posts";

interface Props {
    [key: string]: PostArr[],
}

class PostsBlock extends Component<Props> {
    render() {
        return (
            <>
                {
                   this.props.posts.map((item: any) => (
                        <div key={item.id}>
                            <Post post={item}/>
                        </div>
                    ))
                }
            </>
        )
    }

}

export default PostsBlock;*/

