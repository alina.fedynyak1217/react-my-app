import React from 'react';
import PostArr from "./components/post/Posts";
import PostsBlock from "./components/post/PostsBlock";

const App = () => {
    return (
        <>
            <PostsBlock posts={ PostArr }/>
        </>
    )
};

export default App;

/*import { Component } from 'react';
import PostsBlock from "./components/post/PostsBlock";
import Posts from "./components/post/Posts";

class App extends Component {
    render() {
        return (
            <>
                <PostsBlock posts={ Posts } />
            </>
        )
    }
}

export default App;*/
